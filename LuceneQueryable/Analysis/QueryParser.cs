﻿using System;
using System.Collections.Generic;
using Lucene.Net.Analysis;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using LuceneQueryable.Extensions;
using LuceneQueryable.Schema;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Analysis
{
    public class QueryParser : MultiFieldQueryParser
    {
        #region CONSTRUCTORS

        public QueryParser(Type type, Version matchVersion, Analyzer analyzer)
            : base(matchVersion, SchemaProvider.GetDocumentSchema(type).GetFieldNames(), analyzer)
        {
            _schema = SchemaProvider.GetDocumentSchema(type);
        }

        public QueryParser(Type type, Version matchVersion, string[] fields, Analyzer analyzer, IDictionary<string, float> boosts) : base(matchVersion, fields, analyzer, boosts)
        {
            _schema = SchemaProvider.GetDocumentSchema(type);
        }

        public QueryParser(Type type, Version matchVersion, string[] fields, Analyzer analyzer) : base(matchVersion, fields, analyzer)
        {
            _schema = SchemaProvider.GetDocumentSchema(type);
        }

        private QueryParser(Version matchVersion, string[] fields, Analyzer analyzer, IDictionary<string, float> boosts)
            : base(matchVersion, fields, analyzer, boosts)
        {
        }

        private QueryParser(Version matchVersion, string[] fields, Analyzer analyzer)
            : base(matchVersion, fields, analyzer)
        {
        }

        #endregion

        #region METHODS

        protected override Query GetRangeQuery(string field, string part1, string part2, bool inclusive)
        {
            if (_schema.ContainsKey(field))
            {
                switch (_schema[field].Type)
                {
                    case LuceneFieldType.Int:
                        return NumericRangeQuery.NewIntRange(field, part1.TryCast(0), part2.TryCast(int.MaxValue), inclusive, inclusive);

                    case LuceneFieldType.Long:
                        return NumericRangeQuery.NewLongRange(field, part1.TryCast(0), part2.TryCast(long.MaxValue), inclusive, inclusive);

                    case LuceneFieldType.Float:
                        return NumericRangeQuery.NewFloatRange(field, part1.TryCast(0), part2.TryCast(float.MaxValue), inclusive, inclusive);

                    case LuceneFieldType.Double:
                        return NumericRangeQuery.NewDoubleRange(field, part1.TryCast(0), part2.TryCast(double.MaxValue), inclusive, inclusive);
                }
            }

            return base.GetRangeQuery(field, part1, part2, inclusive);
        }

        #endregion

        #region FIELDS

        private static readonly ILuceneSchemaProvider SchemaProvider = new LuceneSchemaProvider();
        private readonly DocumentSchema _schema;

        #endregion
    }
}