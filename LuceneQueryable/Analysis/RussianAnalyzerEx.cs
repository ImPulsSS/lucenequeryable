﻿using System;
using System.Collections.Generic;
using System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Ru;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Analysis
{
    public class RussianAnalyzerEx : Analyzer
    {
        /// <summary>
        /// List of typical Russian stopwords.
        /// </summary>
        private static readonly String[] RUSSIAN_STOP_WORDS = {
															      "а", "без", "более", "бы", "был", "была", "были",
															      "было", "быть", "в",
															      "вам", "вас", "весь", "во", "вот", "все", "всего",
															      "всех", "вы", "где",
															      "да", "даже", "для", "до", "его", "ее", "ей", "ею",
															      "если", "есть",
															      "еще", "же", "за", "здесь", "и", "из", "или", "им",
															      "их", "к", "как",
															      "ко", "когда", "кто", "ли", "либо", "мне", "может",
															      "мы", "на", "надо",
															      "наш", "не", "него", "нее", "нет", "ни", "них", "но",
															      "ну", "о", "об",
															      "однако", "он", "она", "они", "оно", "от", "очень",
															      "по", "под", "при",
															      "с", "со", "так", "также", "такой", "там", "те", "тем"
															      , "то", "того",
															      "тоже", "той", "только", "том", "ты", "у", "уже",
															      "хотя", "чего", "чей",
															      "чем", "что", "чтобы", "чье", "чья", "эта", "эти",
															      "это", "я"
														      };

        private static class DefaultSetHolder
        {
            internal static readonly ISet<string> DEFAULT_STOP_SET = MakeStopSet(RUSSIAN_STOP_WORDS);
        }

        /// <summary>
        /// Contains the stopwords used with the StopFilter.
        /// </summary>
        private readonly ISet<string> _stopSet;

        private readonly Version _matchVersion;

        private static readonly RussianStemmer Stemmer = new RussianStemmer();


        public RussianAnalyzerEx(Version matchVersion)
            : this(matchVersion, DefaultSetHolder.DEFAULT_STOP_SET)
        {
        }

        /*
         * Builds an analyzer with the given stop words.
         * @deprecated use {@link #RussianAnalyzer(Version, Set)} instead
         */
        public RussianAnalyzerEx(Version matchVersion, params string[] stopwords)
            : this(matchVersion, MakeStopSet(stopwords))
        {

        }

        /*
         * Builds an analyzer with the given stop words
         * 
         * @param matchVersion
         *          lucene compatibility version
         * @param stopwords
         *          a stopword set
         */
        protected RussianAnalyzerEx(Version matchVersion, ISet<string> stopwords)
        {
            _stopSet = CharArraySet.UnmodifiableSet(CharArraySet.Copy(stopwords));
            this._matchVersion = matchVersion;
        }

        /// <summary></summary>
        /// <param name="stopWords">An array of stopwords</param>
        /// <param name="ignoreCase">If true, all words are lower cased first.</param>
        /// <returns> a Set containing the words</returns>
        protected static ISet<string> MakeStopSet(string[] stopWords, bool ignoreCase = false)
        {
            var stopSet = new CharArraySet(stopWords.Length, ignoreCase);

            foreach (var stopWord in stopWords)
            {
                stopSet.Add(Stemmer.Stem(stopWord));
            }

            return stopSet;
        }

        /*
         * Creates a {@link TokenStream} which tokenizes all the text in the 
         * provided {@link Reader}.
         *
         * @return  A {@link TokenStream} built from a 
         *   {@link RussianLetterTokenizer} filtered with 
         *   {@link RussianLowerCaseFilter}, {@link StopFilter}, 
         *   and {@link RussianStemFilter}
         */
        public override TokenStream TokenStream(String fieldName, TextReader reader)
        {
            TokenStream result = new RussianLetterTokenizer(reader);
            result = new LowerCaseFilter(result);
            result = new RussianStemFilter(result);
            result = new StopFilter(StopFilter.GetEnablePositionIncrementsVersionDefault(_matchVersion),
                        result, _stopSet);
            return result;
        }

        private class SavedStreams
        {
            protected internal Tokenizer source;
            protected internal TokenStream result;
        };

        /*
         * Returns a (possibly reused) {@link TokenStream} which tokenizes all the text 
         * in the provided {@link Reader}.
         *
         * @return  A {@link TokenStream} built from a 
         *   {@link RussianLetterTokenizer} filtered with 
         *   {@link RussianLowerCaseFilter}, {@link StopFilter}, 
         *   and {@link RussianStemFilter}
         */
        public override TokenStream ReusableTokenStream(String fieldName, TextReader reader)
        {
            var streams = (SavedStreams)PreviousTokenStream;
            if (streams == null)
            {
                streams = new SavedStreams();
                streams.source = new RussianLetterTokenizer(reader);
                streams.result = new LowerCaseFilter(streams.source);
                streams.result = new RussianStemFilter(streams.result);
                streams.result = new StopFilter(StopFilter.GetEnablePositionIncrementsVersionDefault(_matchVersion),
                                                streams.result, _stopSet);
                PreviousTokenStream = streams;
            }
            else
            {
                streams.source.Reset(reader);
            }
            return streams.result;
        }
    }
}