﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace LuceneQueryable.Utility
{
    public class FormatConverter : TypeConverter
    {
        private string Format { get; set; }

        public FormatConverter(string format)
        {
            Format = format;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            return string.Format("{0:" + Format + "}", value);
        }
    }
}