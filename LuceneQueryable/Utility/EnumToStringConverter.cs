﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace LuceneQueryable.Utility
{
    public class EnumToStringConverter : TypeConverter
    {
        private Type EnumType { get; set; }

        public EnumToStringConverter(Type enumType)
        {
            EnumType = enumType;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string);
        }
        
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            return Enum.GetName(EnumType, value);
        }
    }
}