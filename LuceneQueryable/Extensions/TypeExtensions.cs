﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace LuceneQueryable.Extensions
{
    public static class TypeExtensions
    {
        public static T TryCast<T>(this object value, T defaultValue = default(T), CultureInfo culture = null)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));

            try
            {
                return converter != null
                        ? (T)converter.ConvertFrom(null, culture ?? CultureInfo.InvariantCulture, value)
                        : defaultValue;
            }
            catch
            {
                return defaultValue;
            }
        }

        private static readonly Regex Escape = new Regex(@"([\+\-\&\|\!\(\)\{\}\[\]\^\""\~\*\?\:\\])", RegexOptions.Compiled);
        public static string GetStringValue(this object value, TypeConverter converter, bool strict = true)
        {
            var strValue = converter != null
                ? converter.ConvertToString(value)
                : value != null
                    ? value.ToString()
                    : null;

            strValue = !string.IsNullOrWhiteSpace(strValue)
                ? Escape.Replace(strValue, @"\$1")
                : "*";

            return strict && strValue.Contains(" ") 
                ? "\"" + strValue + "\"" 
                : strValue;
        }
    }
}