﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using LuceneQueryable.Schema;
using LuceneQueryable.Utility;

namespace LuceneQueryable.Extensions
{
    public static class ExpressionExtensions
    {
        public static string GetMemberName(this Expression e)
        {
            if (e is ConstantExpression)
                return ((ConstantExpression)e).Value.ToString();

            if (e is MemberExpression)
            {
                var member = e as MemberExpression;

                if (member.Expression != null && member.Expression.NodeType == ExpressionType.Parameter)
                {
                    var field = member.Member.GetCustomAttributes(typeof(LuceneFieldAttribute), false).FirstOrDefault() as LuceneFieldAttribute;

                    return field != null && !string.IsNullOrWhiteSpace(field.Name)
                        ? field.Name
                        : member.Member.Name.ToLower();
                }
            }

            return null;
        }

        public static LuceneFieldType GetFieldType(this Expression e)
        {
            if (e is MemberExpression)
            {
                var member = e as MemberExpression;

                if (member.Expression != null && member.Expression.NodeType == ExpressionType.Parameter)
                {
                    var field = member.Member.GetCustomAttributes(typeof(LuceneFieldAttribute), false).FirstOrDefault() as LuceneFieldAttribute;

                    return field != null
                        ? field.Type
                        : LuceneFieldType.Text;
                }
            }

            return LuceneFieldType.Text;
        }

        public static object GetMemberValue(this Expression e)
        {
            if (e is ConstantExpression)
                return ((ConstantExpression)e).Value;

            var objectMember = Expression.Convert(e, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();

            return getter();
        }

        public static TypeConverter GetConverter(this Expression e)
        {
            var member = (e is UnaryExpression
                ? ((UnaryExpression)e).Operand
                : e) as MemberExpression;

            if (member == null)
                return null;

            var field = member.Member.GetCustomAttributes(typeof(LuceneFieldAttribute), false)
                              .Cast<LuceneFieldAttribute>()
                              .FirstOrDefault();

            return field != null
                ? field.Converter != null
                    ? Activator.CreateInstance(field.Converter) as TypeConverter
                    : member.Type.IsEnum && field.Type == LuceneFieldType.Text
                        ? new EnumToStringConverter(member.Type)
                        : null
                : null;
        }

        //todo: remove hardcode
        public static bool IsParameterOnLeft(this BinaryExpression binaryExpression)
        {
            if (binaryExpression == null)
                throw new ArgumentNullException("b");

            return (binaryExpression.Left is MemberExpression && ((MemberExpression)binaryExpression.Left).Expression.NodeType == ExpressionType.Parameter) ||
                   (binaryExpression.Left is UnaryExpression && ((UnaryExpression)binaryExpression.Left).Operand is MemberExpression && ((MemberExpression)((UnaryExpression)binaryExpression.Left).Operand).Expression.NodeType == ExpressionType.Parameter);
        }

        public static object GetWrappedValue(this Expression e)
        {
            var result = GetMemberValue(e);

            return result;
        }

        public static Expression StripQuotes(this Expression e)
        {
            while (e.NodeType == ExpressionType.Quote)
            {
                e = ((UnaryExpression)e).Operand;
            }

            return e;
        }
    }
}