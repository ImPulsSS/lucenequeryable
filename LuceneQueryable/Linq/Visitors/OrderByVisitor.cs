﻿using System;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using Lucene.Net.Search;
using LuceneQueryable.Extensions;
using LuceneQueryable.Schema;

namespace LuceneQueryable.Linq.Visitors
{
    public class OrderByVisitor : ILuceneQueryVisitor
    {
        protected virtual bool Direction
        {
            get
            {
                return false;
            }
        }

        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var body = ((LambdaExpression)member.Arguments[1].StripQuotes()).Body;

            if (body.NodeType != ExpressionType.MemberAccess)
            {
                var lambda = Expression.Lambda(body);
                var fn = lambda.Compile();
                body = ((LambdaExpression)fn.DynamicInvoke(null)).Body;
            }

            var field = body.GetMemberName();
            var sortFiled = SortField.STRING;

            switch (body.GetFieldType())
            {
                case LuceneFieldType.Int:
                    sortFiled = SortField.INT;
                    break;

                case LuceneFieldType.Long:
                    sortFiled = SortField.LONG;
                    break;

                case LuceneFieldType.Float:
                    sortFiled = SortField.FLOAT;
                    break;

                case LuceneFieldType.Double:
                    sortFiled = SortField.DOUBLE;
                    break;
            }

            query.Order.Push(new SortField(field, sortFiled, Direction));
        } 
    }

    public class ThenByVisitor : OrderByVisitor
    {
    }

    public class OrderByDescendingVisitor : OrderByVisitor
    {
        protected override bool Direction
        {
            get
            {
                return true;
            }
        }
    }

    public class ThenByDescendingVisitor : OrderByVisitor
    {
        protected override bool Direction
        {
            get
            {
                return true;
            }
        }
    }
}