﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class ContainsVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            if (member.Object != null) // x.Title.Conrtains
            {
                var field = member.Object.GetMemberName();
                var value = member.Arguments[0].GetMemberValue().GetStringValue(member.Object.GetConverter());

                query.SearchQuery.Append(field + ":" + value);
                query.Fields.Add(field);
            }
            else
            {
                if (member.Arguments[0].NodeType == ExpressionType.MemberAccess)
                {
                    var list = member.Arguments[0].GetMemberValue() as IEnumerable;
                    if (list != null)
                    {
                        var field = member.Arguments[1];
                        var fieldName = field.GetMemberName();

                        var values = new List<string>();
                        foreach (var item in list)
                        {
                            values.Add(item.GetStringValue(field.GetConverter()));
                        }

                        query.SearchQuery.Append(string.Join(" OR ", values.Select(v => string.Format("{0}:{1}", fieldName, v))));
                        query.Fields.Add(fieldName);
                    }
                    else
                    {
                        throw new InvalidOperationException("Not supported expression");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Not supported expression");
                }
            }
        } 
    }
}