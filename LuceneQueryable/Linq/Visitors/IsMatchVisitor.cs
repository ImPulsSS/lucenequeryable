﻿using System;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class IsMatchVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var field = member.Arguments[0].GetMemberName();
            var value = member.Arguments[1].GetMemberValue();
            
            if (member.Arguments.Count < 3 || !(bool)member.Arguments[2].GetMemberValue())
                value = value.GetStringValue(member.Arguments[0].GetConverter(), false);
            
            query.SearchQuery.Append(field != null
                ? field + ":" + value
                : value == "*"
                    ? "*:*"
                    : value);

            if (field != null)
                query.Fields.Add(field);
        } 
    }
}