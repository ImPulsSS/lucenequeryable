﻿using System;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class EqualsVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var field = member.Object.GetMemberName();
            var value = member.Arguments[0].GetMemberValue().GetStringValue(member.Object.GetConverter());

            query.SearchQuery.Append(field != null
                ? field + ":" + value
                : value == "*"
                    ? "*:*"
                    : value);

            if (field != null)
                query.Fields.Add(field);
        }
    }
}