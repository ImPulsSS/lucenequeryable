﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class AllVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var operands = ((LambdaExpression)member.Arguments[1].StripQuotes()).Body;

            var binaryOperands = operands as BinaryExpression;
            if (binaryOperands != null && binaryOperands.NodeType == ExpressionType.NotEqual) // x => pArr.All(p => p.id != x.id)
            {
                var isParameterOnLeft = binaryOperands.IsParameterOnLeft();
                var field = isParameterOnLeft ? binaryOperands.Left : binaryOperands.Right;
                var fieldName = field.GetMemberName();

                var list = member.Arguments[0].GetMemberValue() as IEnumerable;
                if (list == null)
                    throw new InvalidOperationException("'All' method was called not for 'IEnumerable'! WTF!?");

                var listItemField = (isParameterOnLeft ? binaryOperands.Right : binaryOperands.Left) as MemberExpression;
                if (listItemField == null)
                    throw new InvalidOperationException("Not supported expression");

                var getterExpression = Expression.Lambda(listItemField, (ParameterExpression)listItemField.Expression);
                var getter = getterExpression.Compile();
                var valuesList = new List<string>();
                foreach (var item in list)
                {
                    valuesList.Add(getter.DynamicInvoke(item).GetStringValue(listItemField.GetConverter()));
                }

                query.SearchQuery.Append(string.Join(" AND ", valuesList.Select(v => string.Format("NOT {0}:{1}", fieldName, v))));
                query.Fields.Add(fieldName);
            }
            else
            {
                var methodCallOperands = operands as MethodCallExpression; // x => x.Tags.All(t => tagIds.Contains(t.id))
                if (methodCallOperands != null)
                {
                    var field = methodCallOperands.Arguments.Last();
                    var fieldName = methodCallOperands.Arguments.Last().GetMemberName();
                    var value = methodCallOperands.Arguments.Count > 1
                        ? methodCallOperands.Arguments[0].GetMemberValue()
                        : methodCallOperands.Object.GetMemberValue();

                    if (value is IEnumerable)
                    {
                        var iterator = (value as IEnumerable).GetEnumerator();
                        var cache = new List<string>();
                        while (iterator.MoveNext())
                        {
                            var item = iterator.Current.GetStringValue(field.GetConverter());
                            cache.Add(fieldName + ":" + item);
                        }

                        if (cache.Count > 0)
                            query.SearchQuery.Append("(" + string.Join(" AND ", cache) + ")");
                    }
                    else
                    {
                        query.SearchQuery.Append(fieldName + ":" + value);
                    }

                    query.Fields.Add(fieldName);
                }
                else
                {
                    throw new ArgumentException("Not supported expression in 'All' method");
                }
            }
        } 
    }
}