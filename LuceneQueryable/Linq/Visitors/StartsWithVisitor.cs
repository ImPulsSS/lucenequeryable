﻿using System;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class StartsWithVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var field = member.Object.GetMemberName();
            var value = member.Arguments[0].GetMemberValue().GetStringValue(member.Object.GetConverter(), false);

            query.SearchQuery.Append(field + ":" + value + "*");

            query.Fields.Add(field);
        } 
    }
}