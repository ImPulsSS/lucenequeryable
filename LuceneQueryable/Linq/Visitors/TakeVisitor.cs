﻿using System;
using System.Linq;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class TakeVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var value = member.Arguments.Last().GetMemberValue();
            
            query.Take = (int)value;
        } 
    }
}