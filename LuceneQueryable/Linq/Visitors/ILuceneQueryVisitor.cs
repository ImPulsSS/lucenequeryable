﻿using System.Linq.Expressions;

namespace LuceneQueryable.Linq.Visitors
{
    public interface ILuceneQueryVisitor
    {
        void Visit(TranslatedQuery query, Expression m);
    }
}