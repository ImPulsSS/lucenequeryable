﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;

namespace LuceneQueryable.Linq.Visitors
{
    public class AnyVisitor : ILuceneQueryVisitor
    {
        public void Visit(TranslatedQuery query, Expression m)
        {
            var member = m as MethodCallExpression;

            if (member == null)
                throw new ArgumentException("method");

            var operands = ((LambdaExpression)member.Arguments[1].StripQuotes()).Body as MethodCallExpression;

            if (operands == null)
                throw new ArgumentException("operands");

            var field = operands.Arguments.Last();
            var fieldName = operands.Arguments.Last().GetMemberName();
            var value = operands.Arguments.Count > 1
                ? operands.Arguments[0].GetMemberValue()
                : operands.Object.GetMemberValue();

            if (value is IEnumerable)
            {
                var iterator = (value as IEnumerable).GetEnumerator();
                var cache = new List<string>();
                while (iterator.MoveNext())
                {
                    var item = iterator.Current.GetStringValue(field.GetConverter());
                    cache.Add(fieldName + ":" + item);
                }

                if (cache.Count > 0)
                    query.SearchQuery.Append("(" + string.Join(" OR ", cache) + ")");
            }
            else
            {
                query.SearchQuery.Append(fieldName + ":" + value);
            }

            query.Fields.Add(fieldName);
        } 
    }
}