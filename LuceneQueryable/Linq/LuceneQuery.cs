﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lucene.Net.Analysis;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Linq
{
    public class LuceneQuery<T> : IOrderedQueryable<T>, IDisposable
    {
        #region CONSTRUCTORS

        public LuceneQuery(LuceneQueryProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        public LuceneQuery(LuceneQueryProvider provider, Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(IQueryable<T>).IsAssignableFrom(expression.Type))
            {
                throw new ArgumentOutOfRangeException("expression");
            }

            _provider = provider;
            _expression = expression;
        }

        public LuceneQuery(Directory directory, Version version)
        {
            _provider = new LuceneQueryProvider(typeof(T), directory, version);
            _expression = Expression.Constant(this);
        }

        public LuceneQuery(Directory directory, Version version, Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            _provider = new LuceneQueryProvider(typeof(T), directory, version);
            _expression = expression;
        }

        public LuceneQuery(Directory directory, Version version, Analyzer analyzer)
        {
            _provider = new LuceneQueryProvider(typeof(T), directory, version, analyzer);
            _expression = Expression.Constant(this);
        }

        public LuceneQuery(Directory directory, Version version, Analyzer analyzer, Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            _provider = new LuceneQueryProvider(typeof(T), directory, version, analyzer);
            _expression = expression;
        }

        #endregion

        #region METHODS

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable)_provider.Execute(_expression)).Cast<T>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_provider.Execute(_expression)).GetEnumerator();
        }

        public TranslatedQuery GetLastQuery()
        {
            return _provider.GetLastQuery();
        }

        #endregion

        #region IDisposable Implementation

        public void Dispose()
        {
            var provider = _provider as LuceneQueryProvider;

            if (provider != null)
            {
                provider.Dispose();
            }
        }

        #endregion

        #region PROPERTIES

        Expression IQueryable.Expression
        {
            get
            {
                return _expression;
            }
        }

        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _provider; }
        }

        #endregion

        #region FIELDS

        readonly LuceneQueryProvider _provider;
        readonly Expression _expression;

        #endregion
    }
}