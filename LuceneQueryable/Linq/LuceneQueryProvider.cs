﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Search;
using Lucene.Net.Store;
using LuceneQueryable.Analysis;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Linq
{
    public class LuceneQueryProvider : IQueryProvider, IDisposable
    {
        #region CONSTRUCTORS

        static LuceneQueryProvider()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) => ((AppDomain)sender).GetAssemblies().FirstOrDefault(asm => asm.FullName == args.Name);
        }

        public LuceneQueryProvider(Type elementType, Directory directory, Version version, Analyzer analyzer)
        {
            IndexSearcher = new IndexSearcher(directory);
            Version = version;
            Analyzer = analyzer;
            ElementType = elementType;
        }

        public LuceneQueryProvider(Type elementType, Directory directory, Version version)
            : this(elementType, directory, version, new StandardAnalyzer(version))
        {
            
        }

        #endregion

        #region METHODS

        public string GetQueryText(Expression expression)
        {
            return Translate(expression).GetOptimized();
        }

        public TranslatedQuery GetLastQuery()
        {
            return _lastQuery;
        }

        public object Execute(Expression expression)
        {
            var query = Translate(expression);
            _lastQuery = query;
            var terms = query.GetOptimized();

            var multiParser = new QueryParser(ElementType, Version, Analyzer);
            var q = !string.IsNullOrWhiteSpace(terms)
                ? multiParser.Parse(terms)
                : new MatchAllDocsQuery();

            var filter = query.Filters.FirstOrDefault() // ChainedFilter is missing
                ?? new QueryWrapperFilter(q);

            var sort = query.Order.Count > 0
                ? new Sort(query.Order.ToArray())
                : Sort.RELEVANCE;

            var result = IndexSearcher.Search(q, filter, query.Skip + query.Take, sort);

            var scoreDocs = result.ScoreDocs;

            var resultType = GetElementType(expression.Type);

            if (resultType == typeof(int))
                return result.TotalHits;

            var converter = System.ComponentModel.TypeDescriptor.GetConverter(ElementType);

            if (typeof(IEnumerable).IsAssignableFrom(expression.Type))
                return scoreDocs
                    .Skip(query.Skip)
                    .Take(query.Take)
                    .Select(x => converter.ConvertFrom(IndexSearcher.Doc(x.Doc)));

            return scoreDocs
                    .Skip(query.Skip)
                    .Take(1)
                    .Select(x => converter.ConvertFrom(IndexSearcher.Doc(x.Doc)))
                    .FirstOrDefault();
        }

        private TranslatedQuery Translate(Expression expression)
        {
            return new LuceneQueryTranslator().Translate(expression);
        }

        private static Type GetElementType(Type seqType)
        {
            var ienum = FindIEnumerable(seqType);

            return ienum == null
                ? seqType
                : ienum.GetGenericArguments()[0];
        }

        private static Type FindIEnumerable(Type seqType)
        {
            if (seqType == null || seqType == typeof(string))
                return null;

            if (seqType.IsArray)
                return typeof(IEnumerable<>).MakeGenericType(seqType.GetElementType());

            if (seqType.IsGenericType)
            {
                foreach (var arg in seqType.GetGenericArguments())
                {
                    var ienum = typeof(IEnumerable<>).MakeGenericType(arg);

                    if (ienum.IsAssignableFrom(seqType))
                    {
                        return ienum;
                    }
                }
            }

            var ifaces = seqType.GetInterfaces();

            if (ifaces.Length > 0)
            {
                foreach (Type iface in ifaces)
                {
                    Type ienum = FindIEnumerable(iface);
                    if (ienum != null) return ienum;
                }
            }

            if (seqType.BaseType != null && seqType.BaseType != typeof(object))
            {
                return FindIEnumerable(seqType.BaseType);
            }

            return null;
        }

        #endregion

        #region PROPERTIES

        public IndexSearcher IndexSearcher { get; set; }
        public Analyzer Analyzer { get; set; }
        public Version Version { get; set; }
        private Type ElementType { get; set; }

        #endregion

        #region FIELDS

        private TranslatedQuery _lastQuery = null;

        #endregion

        #region IQueryProvider Implementation

        IQueryable<T> IQueryProvider.CreateQuery<T>(Expression expression)
        {
            return new LuceneQuery<T>(this, expression);
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            var elementType = GetElementType(expression.Type);

            try
            {
                return (IQueryable)Activator.CreateInstance(typeof(LuceneQuery<>).MakeGenericType(elementType), new object[] { this, expression });
            }
            catch (TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        T IQueryProvider.Execute<T>(Expression expression)
        {
            return (T)Execute(expression);
        }

        object IQueryProvider.Execute(Expression expression)
        {
            return Execute(expression);
        }

        #endregion

        #region IDisposable Implementation

        public void Dispose()
        {
            IndexSearcher.Dispose();
            Analyzer.Dispose();
        }

        #endregion
    }
}