﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Lucene.Net.Search;

namespace LuceneQueryable.Linq
{
    public class TranslatedQuery
    {
        public StringBuilder SearchQuery { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public List<string> Fields { get; set; }
        public Stack<SortField> Order { get; set; }
        public List<Filter> Filters { get; set; }

        public TranslatedQuery()
        {
            Skip = 0;
            Take = 20;
            SearchQuery = new StringBuilder();
            Order = new Stack<SortField>();
            Fields = new List<string>();
            Filters = new List<Filter>();
        }

        public string GetOptimized()
        {
            var query = (SearchQuery.ToString() ?? "");
            query = FullSearchCleaner.Replace(query, "");

            if (string.IsNullOrWhiteSpace(query))
                return null;

            query = RangeParser.Replace(query, "$1:$2$3 TO $5$6$4");
            query = LowerBoundRangeFixer.Replace(query, "$1" + LOWER_BOUND_STRING + "$3");
            query = UpperBoundRangeFixer.Replace(query, "$1" + UPPER_BOUND_STRING + "$3");

            query = AndCleaner.Replace(query, " $1");
            query = OrCleaner.Replace(query, " $1");

            query = EmptyExpressionCleaner.Replace(query, string.Empty);

            return query;
        }

        private static readonly Regex RangeParser = new Regex(@"(?<field>\w+?):(?<lbracer>[\[\{])(?<min>\d+) TO \*[\}\]](?<misc>.*?) AND \1:[\[\{]\* TO (?<max>\d+)(?<rbracer>[\}\]])|(?<field>\w+?):[\[\{]\* TO (?<max>\d+)(?<rbracer>[\}\]])(?<misc>.*?) AND \1:(?<lbracer>[\[\{])(?<min>\d+) TO \*[\}\]]", RegexOptions.Compiled);
        private static readonly Regex LowerBoundRangeFixer = new Regex(@"(?<lbracer>[\[\{])(?<lbound>\*)(?<other> TO)", RegexOptions.Compiled);
        private static readonly Regex UpperBoundRangeFixer = new Regex(@"(?<other>TO )(?<rbound>\*)(?<rbracer>[\]\}])", RegexOptions.Compiled);

        private static readonly Regex FullSearchCleaner = new Regex(@"^\s*\*:\*\s*$|\*:\*\s+(?<operand>AND|OR)\s|\s(?<operand>AND|OR)\s+\*:\*$", RegexOptions.Compiled);
        private static readonly Regex AndCleaner = new Regex(@"(?:\s+(AND)){2,}", RegexOptions.Compiled);
        private static readonly Regex OrCleaner = new Regex(@"(?:\s+(OR)){2,}", RegexOptions.Compiled);

        private static readonly Regex EmptyExpressionCleaner = new Regex(@"(^\s*(OR|AND)\s*)|(\s*(OR|AND)\s*$)", RegexOptions.Compiled);

        internal const string LOWER_BOUND_STRING = "\" \"";
        internal const string UPPER_BOUND_STRING = "\"\uFFDF\"";
    }
}