﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using LuceneQueryable.Extensions;
using LuceneQueryable.Linq.Visitors;

namespace LuceneQueryable.Linq
{
    internal class LuceneQueryTranslator : ExpressionVisitor
    {
        private TranslatedQuery _query;
        private static readonly Dictionary<string, ILuceneQueryVisitor> Visitors;
        private TypeConverter _converter;

        static LuceneQueryTranslator()
        {
            Visitors = AppDomain.CurrentDomain.GetAssemblies()
                                 .SelectMany(x => x.GetTypes())
                                 .Where(x => typeof (ILuceneQueryVisitor).IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface)
                                 .ToDictionary(x => x.Name.Replace("Visitor", ""),
                                               x => Activator.CreateInstance(x) as ILuceneQueryVisitor);
        }

        internal TranslatedQuery Translate(Expression expression)
        {
            _query = new TranslatedQuery();
            _converter = null;

            Visit(expression);

            return _query;
        }

        private void ExecuteVisitor(string name, Expression e)
        {
            if (!Visitors.ContainsKey(name))
                throw new NotSupportedException(string.Format("The method '{0}' is not supported", name));

            Visitors[name].Visit(_query, e);
        }

        protected override Expression VisitMethodCall(MethodCallExpression e)
        {
            switch (e.Method.Name)
            {
                case "Where":
                    if (_query.SearchQuery.Length > 0)
                        _query.SearchQuery.Append(" AND ");

                    Visit(((LambdaExpression) e.Arguments[1].StripQuotes()).Body);
                    break;

                case "Count":
                case "First":
                case "FirstOrDefault":
                /*case "Last":
                case "LastOrDefault":
                case "Single":
                case "SingleOrDefault":*/
                    if (e.Arguments.Count == 2)
                        Visit(Expression.Call(typeof(Queryable), "Where", e.Method.GetGenericArguments(), e.Arguments[0], e.Arguments[1].StripQuotes()));
                    break;

                default:
                    ExecuteVisitor(e.Method.Name, e);
                    break;
            }

            if (e.Arguments[0] is MethodCallExpression)
                Visit(e.Arguments[0]);

            return e;
        }

        protected override Expression VisitUnary(UnaryExpression e)
        {
            switch (e.NodeType)
            {
                case ExpressionType.Not:
                    _query.SearchQuery.Append(" NOT (");
                    Visit(e.Operand);
                    _query.SearchQuery.Append(")");
                    break;

                case ExpressionType.Convert:
                    Visit(e.Operand);
                    break;

                default:
                    throw new NotSupportedException(string.Format("The unary operator '{0}' is not supported", e.NodeType));
            }

            return e;
        }

        protected override Expression VisitNew(NewExpression n)
        {
            var lambda = Expression.Lambda(n);
            var fn = lambda.Compile();
            VisitConstant(Expression.Constant(fn.DynamicInvoke(null), n.Type));

            return n;
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            var isParameterOnLeft = b.IsParameterOnLeft();

            _converter = isParameterOnLeft
                ? b.Left.GetConverter()
                : b.Right.GetConverter();

            switch (b.NodeType)
            {
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                    _query.SearchQuery.Append("(");
                    Visit(b.Left);
                    _query.SearchQuery.Append(" AND ");
                    Visit(b.Right);
                    _query.SearchQuery.Append(")");
                    break;

                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    _query.SearchQuery.Append("(");
                    Visit(b.Left);
                    _query.SearchQuery.Append(" OR ");
                    Visit(b.Right);
                    _query.SearchQuery.Append(")");
                    break;

                case ExpressionType.Equal:
                    Visit(isParameterOnLeft ? b.Left : b.Right);
                    _query.SearchQuery.Append(":");
                    Visit(isParameterOnLeft ? b.Right : b.Left);
                    break;

                case ExpressionType.NotEqual:
                    _query.SearchQuery.Append("-");
                    Visit(isParameterOnLeft ? b.Left : b.Right);
                    _query.SearchQuery.Append(":");
                    Visit(isParameterOnLeft ? b.Right : b.Left);
                    break;

                case ExpressionType.LessThan:
                    if (isParameterOnLeft)
                    {
                        Visit(b.Left);
                        _query.SearchQuery.Append(":{* TO ");
                        Visit(b.Right);
                        _query.SearchQuery.Append("}");
                    }
                    else
                    {
                        Visit(b.Right);
                        _query.SearchQuery.Append(":{");
                        Visit(b.Left);
                        _query.SearchQuery.Append(" TO *}");
                    }
                    break;

                case ExpressionType.LessThanOrEqual:
                    if (isParameterOnLeft)
                    {
                        Visit(b.Left);
                        _query.SearchQuery.Append(":[* TO ");
                        Visit(b.Right);
                        _query.SearchQuery.Append("]");
                    }
                    else
                    {
                        Visit(b.Right);
                        _query.SearchQuery.Append(":[");
                        Visit(b.Left);
                        _query.SearchQuery.Append(" TO *]");
                    }
                    break;

                case ExpressionType.GreaterThan:
                    if (isParameterOnLeft)
                    {
                        Visit(b.Left);
                        _query.SearchQuery.Append(":{");
                        Visit(b.Right);
                        _query.SearchQuery.Append(" TO *}");
                    }
                    else
                    {
                        Visit(b.Right);
                        _query.SearchQuery.Append(":{* TO ");
                        Visit(b.Left);
                        _query.SearchQuery.Append("}");
                    }
                    break;

                case ExpressionType.GreaterThanOrEqual:
                    if (isParameterOnLeft)
                    {
                        Visit(b.Left);
                        _query.SearchQuery.Append(":[");
                        Visit(b.Right);
                        _query.SearchQuery.Append(" TO *]");
                    }
                    else
                    {
                        Visit(b.Right);
                        _query.SearchQuery.Append(":[* TO ");
                        Visit(b.Left);
                        _query.SearchQuery.Append("]");
                    }
                    break;

                default:
                    throw new NotSupportedException(string.Format("The binary operator '{0}' is not supported", b.NodeType));
            }

            return b;
        }

        protected override Expression VisitConstant(ConstantExpression c)
        {
            _query.SearchQuery.Append(c.Value.GetStringValue(_converter));

            return c;
        }

        protected override Expression VisitMember(MemberExpression m)
        {
            if (m.Expression != null && m.Expression.NodeType == ExpressionType.Parameter)
            {
                _query.SearchQuery.Append(m.GetMemberName());
				
	            var isBoolField = false;
	            if (m.Member.MemberType == MemberTypes.Property)
	            {
		            isBoolField = (((PropertyInfo) m.Member).PropertyType == typeof (bool));
	            }
				else if (m.Member.MemberType == MemberTypes.Field)
				{
					isBoolField = ((FieldInfo) m.Member).FieldType == typeof (bool);
				}

	            if (isBoolField)
	            {
		            _query.SearchQuery.Append(":1");
	            }
            }
            else
            {
                var lambda = Expression.Lambda(m);
                var fn = lambda.Compile();
                VisitConstant(Expression.Constant(fn.DynamicInvoke(null), m.Type));
            }

            return m;
        }
    }
}