﻿using System;

namespace LuceneQueryable.Schema
{
    public interface ILuceneSchemaProvider
    {
        DocumentSchema GetDocumentSchema(Type type);
    }
}