﻿using System.Collections.Generic;
using System.Linq;

namespace LuceneQueryable.Schema
{
    public class DocumentSchema : Dictionary<string, LuceneField>
    {
        public DocumentSchema(IEnumerable<KeyValuePair<string, LuceneField>> fields)
        {
            foreach (var field in fields)
            {
                if (ContainsKey(field.Key))
                    continue;
                
                Add(field.Key, field.Value);
            }
        }

        public string[] GetFieldNames()
        {
            return Keys.ToArray();
        }
    }
}