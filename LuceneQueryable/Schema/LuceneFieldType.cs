﻿namespace LuceneQueryable.Schema
{
    public enum LuceneFieldType
    {
        Text,
        Int,
        Long,
        Float,
        Double
    }
}