﻿using System;
using System.ComponentModel;
using System.Reflection;
using LuceneQueryable.Utility;

namespace LuceneQueryable.Schema
{
    public class LuceneField
    {
        public string Name { get; set; }
        public LuceneFieldType Type { get; set; }
        public TypeConverter Converter { get; set; }
        public PropertyInfo Property { get; set; }

        public LuceneField(LuceneFieldAttribute field, PropertyInfo property)
        {
            Name = field.Name ?? property.Name.ToLower();
            Type = field.Type;

            Converter = field.Converter != null
                ? Activator.CreateInstance(field.Converter) as TypeConverter
                : property.PropertyType.IsEnum && field.Type == LuceneFieldType.Text
                    ? new EnumToStringConverter(property.PropertyType)
                    : null;

            Property = property;
        }
    }
}