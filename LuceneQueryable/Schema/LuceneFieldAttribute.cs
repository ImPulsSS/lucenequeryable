﻿using System;

namespace LuceneQueryable.Schema
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LuceneFieldAttribute : Attribute
    {
        public string Name { get; set; }
        public Type Converter { get; set; }
        public LuceneFieldType Type { get; set; }
    }
}