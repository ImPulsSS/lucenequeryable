﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace LuceneQueryable.Schema
{
    public class LuceneSchemaProvider : ILuceneSchemaProvider
    {
        public DocumentSchema GetDocumentSchema(Type type)
        {
            DocumentSchema result;

            if (_schemaCache.TryGetValue(type, out result))
                return result;

            var fields = type.GetProperties()
                         .Where(x => Attribute.IsDefined(x, typeof(LuceneFieldAttribute), false))
                         .Select(x => new
                         {
                             Property = x,
                             Field = (LuceneFieldAttribute)Attribute.GetCustomAttribute(x, typeof(LuceneFieldAttribute), false)
                         })
                         .GroupBy(x => x.Field.Name ?? x.Property.Name.ToLower())
                         .ToDictionary(x => x.Key, x => new LuceneField(x.First().Field, x.First().Property));

            result = new DocumentSchema(fields);
            _schemaCache.AddOrUpdate(type, result, (t, schema) => result);

            return result;
        }

        private readonly ConcurrentDictionary<Type, DocumentSchema> _schemaCache = new ConcurrentDictionary<Type, DocumentSchema>();
    }
}