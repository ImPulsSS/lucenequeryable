﻿using System.Linq;
using Lucene.Net.Store;
using LuceneQueryable.Analysis;
using LuceneQueryable.Extensions;
using LuceneQueryable.Linq;
using LuceneQueryable.Schema;
using LuceneQueryable.Tests.Models;
using NUnit.Framework;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Tests
{
    public class RussianAnalyzerExTests
    {
        private LuceneQuery<ObjectInfo> _catalog;
        private QueryParser _multiParser;

        [SetUp]
        public void Init()
        {
            var directory = FSDirectory.Open(@"d:\temp\lucene");

            var analyzer = new RussianAnalyzerEx(Version.LUCENE_30, 
                "а", "без", "более", "бы", "был", "была", "были",
			    "было", "быть", "в",
			    "вам", "вас", "весь", "во", "вот", "все", "всего",
			    "всех", "вы", "где",
			    "да", "даже", "для", "до", "его", "ее", "ей", "ею",
			    "если", "есть",
			    "еще", "же", "за", "здесь", "и", "из", "или", "им",
			    "их", "к", "как",
			    "ко", "когда", "кто", "ли", "либо", "мне", "может",
			    "мы", "на", "надо",
			    "наш", "не", "него", "нее", "нет", "ни", "них", "но",
			    "ну", "о", "об",
			    "однако", "он", "она", "они", "оно", "от", "очень",
			    "по", "под", "при",
			    "с", "со", "так", "также", "такой", "там", "те", "тем"
			    , "то", "того",
			    "тоже", "той", "только", "том", "ты", "у", "уже",
			    "хотя", "чего", "чей",
			    "чем", "что", "чтобы", "чье", "чья", "эта", "эти",
			    "это", "я",

                "купить", "новый", "год", "бесплатный", "смс", "дешевый");

            _catalog = new LuceneQuery<ObjectInfo>(directory, Version.LUCENE_30, analyzer);
            _multiParser = new QueryParser(typeof(TestObj), Version.LUCENE_30, analyzer);
        }

        [Test]
        public void First()
        {
            var res = _catalog
                .Where(x => x.IsMatch("купи обувь на новый год бесплатно без смс"))
                .Count();

            var query = _multiParser.Parse(_catalog.GetLastQuery().GetOptimized());
            Assert.AreEqual("(title:обув)", query.ToString());
        }

        [Test]
        public void Second()
        {
            var res = _catalog
                .Where(x => x.IsMatch("купить бесплатную одежду дешево"))
                .Count();

            var query = _multiParser.Parse(_catalog.GetLastQuery().GetOptimized());
            Assert.AreEqual("(title:одежд)", query.ToString());
        }
    }

    public class TestObj
    {
        [LuceneField]
        public string Title { get; set; }
    }
}