﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Lucene.Net.Documents;
using LuceneQueryable.Schema;
using LuceneQueryable.Utility;
using Newtonsoft.Json;

namespace LuceneQueryable.Tests.Models
{
    [TypeConverter(typeof(ObjectInfoConverter))]
    public class ObjectInfo
    {
        #region CONSTRUCTORS

        public ObjectInfo()
        {
            CreationDate = DateTime.Now;
            UpdateDate = DateTime.Now;
        }

        #endregion

        #region PROPERTIES

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public DateTime? StartDate { get; set; }
        [LuceneField(Name = "enddate", Type = LuceneFieldType.Text, Converter = typeof(LuceneDateConverter))]
        public DateTime? EndDate { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        [LuceneField(Name = "urlidentity")]
        public string UrlIdentifier
        {
            get
            {
                return CleanUrlKey + (Index == 0 ? string.Empty : "-" + Index);
            }
        }
        public string CleanUrlKey { get; set; }

        public int Index { get; set; }

        public string Url { get; set; }

        public string PromoUrl { get; set; }
        public string DiscountText { get; set; }
        [LuceneField(Name = "type", Type = LuceneFieldType.Text)]
        public ObjectType Type { get; set; }

        [LuceneField(Name = "type", Type = LuceneFieldType.Int)]
        public ObjectType IntType { get; set; }

		public bool IsActive { get; set; }

		public bool ShowOnSite { get; set; }

        public List<ObjectOption> Categories { get; set; }
        public List<ObjectOption> Shops { get; set; }

        public List<ObjectOption> Aliases { get; set; }

        [LuceneField]
        public List<ObjectOption> Tags { get; set; }

        public string Picture { get; set; }
        public List<string> Pictures { get; set; }
        public int NetworkId { get; set; }

        public string NetworkUid { get; set; }

        public string OriginalData { get; set; }

        [LuceneField(Name = "floatprice", Type = LuceneFieldType.Float)]
        public decimal Price { get; set; }

        public List<ObjectOption> Brands { get; set; }

        public string PromoCode { get; set; }

        [LuceneField(Name = "floatprice", Type = LuceneFieldType.Double)]
        public double Rating { get; set; }

        public decimal? OldPrice { get; set; }

        [LuceneField(Name = "discount", Type = LuceneFieldType.Int)]
        public int? DiscountPercent { get; set; }

        #endregion
    }

    public class ObjectInfoConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return false;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(Document);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var doc = value as Document;

            if (doc == null)
                return null;

            var source = doc.GetField("source").StringValue;

            if (string.IsNullOrWhiteSpace(source))
                return null;

            return JsonConvert.DeserializeObject<ObjectInfo>(source);
        }
    }

    public class LuceneDateConverter : FormatConverter
    {
        public LuceneDateConverter() : base("yyyyMMdd")
        {
        }
    }
}