﻿using LuceneQueryable.Schema;

namespace LuceneQueryable.Tests.Models
{
    public class ObjectOption
    {
        [LuceneField(Name = "tagid")]
        public int Id { get; set; }

        [LuceneField(Name = "category")]
        public string Title { get; set; }
    }
}