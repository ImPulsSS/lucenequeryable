﻿namespace LuceneQueryable.Tests.Models
{
    public enum ObjectType
    {
        None,
        Coupon,
        Product,
        Stat
    }
}