﻿using System;
using System.Linq;
using System.Linq.Expressions;
using LuceneQueryable.Extensions;
using LuceneQueryable.Linq;
using LuceneQueryable.Tests.Models;
using NUnit.Framework;

namespace LuceneQueryable.Tests
{
    [TestFixture]
    public class LuceneQueryTranslatorTests
    {
        [Test]
        public void Search()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IsMatch("test");

            Assert.AreEqual("test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void SearchNull()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IsMatch(null);

            Assert.AreEqual(null, new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void SearchEmpty()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IsMatch("");

            Assert.AreEqual(null, new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void SearchUnsafe()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IsMatch("\"test\"~2", true);

            Assert.AreEqual("\"test\"~2", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void SearchSpecialCharacters()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IsMatch(@"+ - & | ! ( ) { } [ ] ^ "" ~ * ? : \");
            Assert.AreEqual(@"\+ \- \& \| \! \( \) \{ \} \[ \] \^ \"" \~ \* \? \: \\", new LuceneQueryTranslator().Translate(ex).GetOptimized());

            ex = x => x.Equals(@"+ - & | ! ( ) { } [ ] ^ "" ~ * ? : \");
            Assert.AreEqual(@"""\+ \- \& \| \! \( \) \{ \} \[ \] \^ \"" \~ \* \? \: \\""", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void SearchLogicalOperators()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => (x.IsMatch("test") || x.Title.StartsWith("test")) && x.Type == ObjectType.Product;

            Assert.AreEqual("((test OR title:test*) AND type:Product)", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringEquals()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title == "test";

            Assert.AreEqual("title:test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringNotEquals()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title != "test";

            Assert.AreEqual("-title:test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringEqualsSwappedOperands()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => "test" == x.Title;

            Assert.AreEqual("title:test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringEqualsPhraze()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title == "black `n white";

            Assert.AreEqual("title:\"black `n white\"", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringEqualsVariable()
        {
            var title = "test";
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title == title;

            Assert.AreEqual("title:test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringContains()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title.Contains("test");

            Assert.AreEqual("title:test", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringStartsWith()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title.StartsWith("test");

            Assert.AreEqual("title:test*", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void StringEndsWith()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Title.EndsWith("test");

            Assert.Throws<NotSupportedException>(() => new LuceneQueryTranslator().Translate(ex)); // lucene doesn't support the wildcard at the beginning
        }

        [Test]
        public void InAll()
        {
            var tags = new[] { 1, 2, 3 };
            Expression<Func<ObjectInfo, bool>> ex = x => x.Tags.All(tag => tags.Contains(tag.Id));

            Assert.AreEqual("(tagid:1 AND tagid:2 AND tagid:3)", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void InAny()
        {
            var tags = new[] { 1, 2, 3 };
            Expression<Func<ObjectInfo, bool>> ex = x => x.Tags.Any(tag => tags.Contains(tag.Id));

            Assert.AreEqual("(tagid:1 OR tagid:2 OR tagid:3)", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void NotIn_Linq()
        {
            var except = new[] {new {key = "1"}, new {key = "2"}, new {key = "3"}};
            Expression<Func<ObjectInfo, bool>> ex = x => except.All(t => x.UrlIdentifier != t.key);

            Assert.AreEqual("NOT urlidentity:1 AND NOT urlidentity:2 AND NOT urlidentity:3", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void NotIn_Contains()
        {
            var except = new[] { "1", "2", "3" };
            Expression<Func<ObjectInfo, bool>> ex = x => !except.Contains(x.UrlIdentifier);

            Assert.AreEqual(" NOT (urlidentity:1 OR urlidentity:2 OR urlidentity:3)", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void Not()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => !(x.Title == "test");

            Assert.AreEqual(" NOT (title:test)", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void Greater()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price > 0;

            Assert.AreEqual("floatprice:{0 TO " + TranslatedQuery.UPPER_BOUND_STRING + "}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void GreaterSwappedOperands()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => 0 < x.Price;

            Assert.AreEqual("floatprice:{0 TO " + TranslatedQuery.UPPER_BOUND_STRING + "}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void GreaterThanDateTime()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.EndDate > new DateTime(2013, 12, 10);

            Assert.AreEqual("enddate:{20131210 TO " + TranslatedQuery.UPPER_BOUND_STRING + "}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void GreaterThanDateTimeNow()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.EndDate >= DateTime.Now;

            new LuceneQueryTranslator().Translate(ex).GetOptimized();
        }

        [Test]
        public void GreaterThanDateTimeVariable()
        {
            var date = new DateTime(2013, 12, 10);
            Expression<Func<ObjectInfo, bool>> ex = x => x.EndDate > date;

            Assert.AreEqual("enddate:{20131210 TO " + TranslatedQuery.UPPER_BOUND_STRING + "}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void GreaterOrEqual()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price >= 0;

            Assert.AreEqual("floatprice:[0 TO " + TranslatedQuery.UPPER_BOUND_STRING + "]", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void Less()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price < 1000;

            Assert.AreEqual("floatprice:{" + TranslatedQuery.LOWER_BOUND_STRING + " TO 1000}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void LessSwappedOperands()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => 1000 > x.Price;

            Assert.AreEqual("floatprice:{" + TranslatedQuery.LOWER_BOUND_STRING + " TO 1000}", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void LessOrEqual()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price <= 1000;

            Assert.AreEqual("floatprice:[" + TranslatedQuery.LOWER_BOUND_STRING + " TO 1000]", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void Between()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price > 0 && x.Price < 1000;

            Assert.AreEqual("(floatprice:{0 TO 1000})", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void InclusiveBetween()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Price >= 0 && x.Price <= 1000;

            Assert.AreEqual("(floatprice:[0 TO 1000])", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void EnumByName()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.Type == ObjectType.Product;

            Assert.AreEqual("type:Product", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void EnumByNameSwappedOperands()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => ObjectType.Product == x.Type;

            Assert.AreEqual("type:Product", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

        [Test]
        public void EnumByValue()
        {
            Expression<Func<ObjectInfo, bool>> ex = x => x.IntType == ObjectType.Coupon;

            Assert.AreEqual("type:1", new LuceneQueryTranslator().Translate(ex).GetOptimized());
        }

		[Test]
	    public void Bool()
	    {
			Expression<Func<ObjectInfo, bool>> ex = x => x.IsActive;

			Assert.AreEqual("isactive:1", new LuceneQueryTranslator().Translate(ex).GetOptimized());

			Expression<Func<ObjectInfo, bool>> ex2 = x => x.IsActive && x.ShowOnSite;

			Assert.AreEqual("(isactive:1 AND showonsite:1)", new LuceneQueryTranslator().Translate(ex2).GetOptimized());

			Expression<Func<ObjectInfo, bool>> ex3 = x => x.IsActive && !x.ShowOnSite;

			Assert.AreEqual("(isactive:1 AND  NOT (showonsite:1))", new LuceneQueryTranslator().Translate(ex3).GetOptimized());
	    }
    }
}