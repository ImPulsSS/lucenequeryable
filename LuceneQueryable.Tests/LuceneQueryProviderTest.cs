﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lucene.Net.Analysis.Ru;
using Lucene.Net.Search;
using Lucene.Net.Store;
using LuceneQueryable.Extensions;
using LuceneQueryable.Linq;
using LuceneQueryable.Tests.Models;
using NUnit.Framework;
using Version = Lucene.Net.Util.Version;

namespace LuceneQueryable.Tests
{
    [TestFixture]
    public class LuceneQueryProviderTest
    {
        private LuceneQuery<ObjectInfo> _catalog;

        [SetUp]
        public void Init()
        {
            var directory = FSDirectory.Open(@"d:\temp\lucene");

            _catalog = new LuceneQuery<ObjectInfo>(directory, Version.LUCENE_30, new RussianAnalyzer(Version.LUCENE_30));
        }

        [Test]
        public void Where()
        {
            var res = _catalog
                .Where(x => x.IsMatch("обувь"))
                .Where(x => x.IsMatch(null))
                .Where(x => x.Type == ObjectType.Product && x.Price >= 100)
                .ToList();

            Assert.NotNull(res);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("(type:Product AND floatprice:[100 TO " + TranslatedQuery.UPPER_BOUND_STRING + "]) AND обувь", query.GetOptimized()); // reverse order in multiple where
        }

        [Test]
        public void WhereWithEmptyExpressions()
        {
            var array = new int[] {};
            var res = _catalog
                .Where(x => x.IsMatch(null))
                .Where(x => x.Tags.All(t => array.Contains(t.Id)))
                .Where(x => x.Type == ObjectType.Product)
                .Where(x => x.Tags.All(t => array.Contains(t.Id)))
                .Where(x => x.IsMatch(null))
                .ToList();

            Assert.NotNull(res);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("type:Product", query.GetOptimized());
        }

        [Test]
        public void Order()
        {
            var res = _catalog
                .Where(x => x.Type == ObjectType.Product)
                .OrderBy(x => x.Title)
                .ThenBy(x => x.Description)
                .ThenByDescending(x => x.Price)
                .ToList();

            Assert.NotNull(res);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual(new List<SortField>
                            {
                                new SortField("title", SortField.STRING),
                                new SortField("description", SortField.STRING),
                                new SortField("floatprice", SortField.FLOAT, true)
                            }, query.Order);
        }

        private Expression ParamToOrderByExpression(string propertyName)
        {
            var param = Expression.Parameter(typeof(ObjectInfo), "model");
            var property = Expression.Property(param, propertyName);

            var delegateType = typeof(Func<,>).MakeGenericType(typeof(ObjectInfo), property.Type);

            return Expression.Lambda(delegateType, property, param);
        }

        [Test]
        public void OrderExpression()
        {
            var res = _catalog
                .OrderBy(x => ParamToOrderByExpression("Title"))
                .ThenBy(x => ParamToOrderByExpression("Description"))
                .ThenByDescending(x => ParamToOrderByExpression("Price"))
                .Count();

            Assert.True(res > 0);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual(new List<SortField>
                            {
                                new SortField("title", SortField.STRING),
                                new SortField("description", SortField.STRING),
                                new SortField("floatprice", SortField.FLOAT, true)
                            }, query.Order);
        }

        [Test]
        public void SkipTake()
        {
            var res = _catalog
                .Skip(10)
                .Take(5)
                .ToList();

            Assert.NotNull(res);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual(10, query.Skip);
            Assert.AreEqual(5, query.Take);
        }

        [Test]
        public void Translate()
        {
            var res = _catalog
                .Where(x => x.IsMatch("обувь") && x.Type == ObjectType.Product && 100 <= x.Price)
                .OrderBy(x => x.Title)
                .ThenByDescending(x => x.Price)
                .Skip(10)
                .Take(5)
                .ToList();

            Assert.NotNull(res);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("((обувь AND type:Product) AND floatprice:[100 TO " + TranslatedQuery.UPPER_BOUND_STRING + "])", query.GetOptimized());
            Assert.AreEqual(10, query.Skip);
            Assert.AreEqual(5, query.Take);
            Assert.AreEqual(new List<SortField>
                            {
                                new SortField("title", SortField.STRING),
                                new SortField("floatprice", SortField.FLOAT, true)
                            }, query.Order);
        }

        [Test]
        public void Count()
        {
            var res1 = _catalog.Where(x => x.Type == ObjectType.Coupon && x.IsMatch("Apart"))
                               .Count();

            var res2 = _catalog.Count(x => x.Type == ObjectType.Coupon && x.IsMatch("Apart"));

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("(type:Coupon AND Apart)", query.GetOptimized());

            Assert.True(res1 > 0);
            Assert.AreEqual(res1, res2);
        }

        [Test]
        public void First()
        {
            var res1 = _catalog.Where(x => x.Type == ObjectType.Product)
                               .First();

            var res2 = _catalog.First(x => x.Type == ObjectType.Product);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("type:Product", query.GetOptimized());

            Assert.NotNull(res1);
            Assert.AreEqual(res1.UrlIdentifier, res2.UrlIdentifier);
        }

        [Test]
        public void PartialApply()
        {
            var res1 = _catalog.Where(x => x.IsMatch("обувь") && x.Type == ObjectType.Product && 100 <= x.Price);

            res1 = res1.OrderBy(x => x.Title)
                       .ThenByDescending(x => x.Price);

            res1 = res1.Skip(10).Take(5);

            var res2 = res1.ToList();

            Assert.NotNull(res2);

            var query = _catalog.GetLastQuery();
            Assert.AreEqual("((обувь AND type:Product) AND floatprice:[100 TO " + TranslatedQuery.UPPER_BOUND_STRING + "])", query.GetOptimized());
            Assert.AreEqual(10, query.Skip);
            Assert.AreEqual(5, query.Take);
            Assert.AreEqual(new List<SortField>
                            {
                                new SortField("title", SortField.STRING),
                                new SortField("floatprice", SortField.FLOAT, true)
                            }, query.Order);
        }
    }
}